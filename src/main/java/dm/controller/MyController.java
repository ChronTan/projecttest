package dm.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import dm.model.Car;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class MyController {
    private List<Car> car=createList();

    @RequestMapping(value ="/car", method= RequestMethod.GET ,produces="application/json")
    public List<Car> carPage(){
        return car;
    }

    private static List<Car> createList() {
        List<Car> tempCar = new ArrayList<>();
        Car car1 = new Car();
        car1.setId(1);
        car1.setName("BMW");
        car1.setPrice(800000);

        Car car2 = new Car();
        car2.setId(2);
        car2.setName("Audi");
        car2.setPrice(700000);

        Car car3 = new Car();
        car3.setId(3);
        car3.setName("Porsche");
        car3.setPrice(750000);

        tempCar.add(car1);
        tempCar.add(car2);
        tempCar.add(car3);
        return tempCar;
    }
}
